

/**
 * Created by Адель on 28.02.2015.
 */
public interface IList {
    void add(int value);
    void add(int index, int value);
    void remove(int index);
    int get(int index);
    int size();
}
