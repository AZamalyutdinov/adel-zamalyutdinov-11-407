/**
 * Created by Адель on 21.03.2015.
 */
class Link {
    private Link next;
    private  int data;
    public Link(int data){
        this.data=data;
    }
    public void setNext(Link nextLink) {
        next = nextLink;
    }
    public Link getNext() {
        return next;
    }
    public int getData() {
        return data;
    }
}
public class LinkedList implements IList {
    Link first;
    Link last;

    @Override
    public void add(int value) {
        Link nextLink = new Link(value);
        if (first == null) {
            first = nextLink;
        } else {
            findLast().setNext(nextLink);
        }
    }

    private Link findLast() {
        Link link = first;
        while (link.getNext() != null) {
            link = link.getNext();
        }
        return link;
    }

    private Link findByIndex(int index) {
        Link link = first;
        if (index<size()) {
            for (int i = 0; i < index; i++) {
                if (link.getNext() == null) {
                    break;
                }
                link = link.getNext();
            }
            return link;
        } else throw new IndexOutOfBoundsException("Index " + index + ", size is " + size());
    }

    @Override
    public void add(int index, int value) {
        int size = size();
        if(index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index " + index + ", size is " + size);
        }
        Link x = first;
        Link prev = null;
        for (int i = 0; i < index; i++) {
            prev = x;
            x = x.getNext();
        }
        Link n = new Link(value);
        n.setNext(x);
        if(prev != null) {
            prev.setNext(n);
        } else {
            first = n;
        }
        if(x == null) {
            last = n;
        }
        size++;
    }

    @Override
    public void remove(int index) {
        int size = size();
        if ((index >= size) || (index < 0)) throw new IndexOutOfBoundsException("Index " + index + ", size is " + size);
        Link x = first;
        Link prev = null;
        for (int i = 0; i < index; i++) {
            prev = x;
            x = x.getNext();
        }

        if (x.getNext() == null) {
            last = prev;
        }

        if (prev != null) {
            prev.setNext(x.getNext());
        } else {
            first = x.getNext();
        }
        size--;
    }

    @Override
    public int get(int index) {
        Link l = findByIndex(index);
        return l.getData();
    }

    @Override
    public int size() {
        int size = 0;
        Link link = first;
        while (link != null) {
            size++;
            link = link.getNext();
        }
        return size;
    }
}
