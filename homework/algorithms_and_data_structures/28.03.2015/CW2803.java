/**
 * Created by Адель on 28.03.2015.
 */
    class Tree {
    private Node root;

    public Tree() {
        root = null;
    }
    public void add(int key, double value) {
        Node node = new Node(key, value);
        if (root==null) {root=node;}
        else {
            Node element = root;
            Node parent;
            while (true) {
                parent = element;
                if (key < element.getKey()) {
                    element = element.getLeft();
                    if (element == null) {
                        parent.setLeft(node);
                        return;
                    }
                } else {
                    element = element.getRight();
                    if (element == null) {
                        parent.setRight(node);
                        return;
                    }
                }
            }
        }
    }
    private Node getChild(Node node){
        Node childParent = node;
        Node child = node;
        Node element = node.getRight();
        while(element!=null) {
            childParent = child;
            child = element;
            element = element.getLeft();
        }
        if(child!=node.getRight()){
            childParent.setLeft(child.getRight());
            child.setRight(node.getRight());
        }
        return child;
    }
    public boolean remove(int key) {
        Node element = root;
        Node parent = root;
        boolean left = true;
        while (element.getKey() != key) {
            parent = element;
            if (key < element.getKey()) {
                left = true;
                element = element.getLeft();
            } else {
                left = false;
                element = element.getRight();
            }
            if (element == null) {
                return false;
            }
        }
        if ((element.getLeft() == null) && (element.getRight() == null)) {
            if (element == root) {
                root = null;
            } else {
                if (left) {
                    parent.setLeft(null);
                } else {
                    parent.setRight(null);
                }
            }
            return true;
        }
        if (element.getRight() == null || element.getLeft() == null) {
            if ((element == root) && (element.getRight() == null)) {
                root = element.getLeft();
            } else {
                root = element.getRight();
            }
            if (left) {
                parent.setLeft(element.getLeft());
            } else {
                parent.setRight(element.getRight());
            }
            return true;
        } else {
            Node child = getChild(element);
            if (element == root) {
                root = child;
            }
            if (left) {
                parent.setLeft(child);
            } else {
                parent.setRight(child);
            }
        }
        return true;
    }
    public Node getElement(int key) {
    Node element =root;
        while (element.getKey()!=key) {
            if (key < element.getKey()) {
                element = element.getLeft();
            } else {
                element = element.getRight();
            }
            if (element == null) {
                return null;
            }
        }
            return element;
        }
    }

    class Node {
        private  Node left;
        private  Node right;
        private  double value;
        private int key;
        public Node getLeft (){
            return left;
        }
        public Node getRight(){
            return right;
        }
        public int getKey() {
            return key;
        }
        public double getValue() {
            return value;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public void setRight(Node right) {
            this.right = right;
        }

        Node(int key, double value){
            this.key = key;
            this.value = value;

        }
    }
