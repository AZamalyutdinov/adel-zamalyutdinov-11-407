/**
 * Created by Адель on 28.03.2015.
 */
interface MyTree {
    public void add(int key, double value);
    public Node getChild(Node node);
    public boolean remove(int key);
    public Node getElement(int key);
}
