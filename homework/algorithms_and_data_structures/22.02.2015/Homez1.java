import com.sun.org.apache.xpath.internal.SourceTree;
import com.sun.security.auth.SolarisNumericUserPrincipal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by Адель on 17.02.2015.
 */
public class Homez1 {
    public static void main(String[] args) {
        long s =0;
        s= System.currentTimeMillis();
        Scanner sc = new Scanner(System.in);
        ArrayList<Number> list = new ArrayList<Number>();
        int n = sc.nextInt();
        list.add(new Number(0, 1, 1));
        list.add(new Number(1, 1, 0));
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < list.size() - 1; j += 2) {
                int next = list.get(j).getNext();
                int sh = list.get(j).getSh() + list.get(next).getSh();
                int zn = list.get(j).getZn() + list.get(next).getZn();
                if (zh <= n) {
                    list.add(new Number(sh, zn, next));
                    list.get(j).setNext(list.size() - 1);
                } else {
                    j--;
                }
            }
        }
        int next = list.get(0).getNext();
        System.out.print(list.get(0).getSh() + "/" + list.get(0).getZn() + " ; ");
        while (next != 0) {
            System.out.print(list.get(next).getSh() + "/" + list.get(next).getZn() + " ; ");
            next = list.get(next).getNext();
        }
        s =System.currentTimeMillis()-s;
        System.out.println("Программа выполняется за "+s);
    }

    public static class Number {
        int sh;
        int zn;
        int next;

        public int getSh() {return sh;}

        public int getZn() {return zn;}

        public int getNext() {return next;}

        public void setNext(int next) {this.next = next;}

        public Number(int sh, int zn, int next) {
            this.sh = sh;
            this.zn = zn;
            this.next = next;
        }
    }
}
