/**
 * Created by Адель on 02.03.2015.
 */
public interface MyQueue {
    void add(int value);
    int get();
    int size();
    void print();
}
