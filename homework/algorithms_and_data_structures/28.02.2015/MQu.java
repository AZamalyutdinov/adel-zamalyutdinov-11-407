/**
 * Created by Адель on 03.03.2015.
 */
public class MQu implements MyQueue{
    private int[] a;
    MQu() {
        a= new int[0];
    }
    @Override
    public void add(int value) {
        int[] c = new int[a.length];
        c=a;
        System.arraycopy(a,0, c, 0, a.length);
        a = new int[a.length+1];
        System.arraycopy(c,0, a, 0, c.length);
        a[a.length-1]=value;
    }

    @Override
    public int get() {
        int m = a[0];
        int c[] = new int[a.length-1];
        System.arraycopy(a,1, c, 0, a.length-1);
        a= new int[a.length-1];
        a=c;
        return m;
    }

    @Override
    public int size() {
        return a.length;
    }

    @Override
    public void print() {
        for (int i=0; i<a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }
}