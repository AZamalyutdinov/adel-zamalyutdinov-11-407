import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Адель on 18.03.2015.
 */
public class HW1403 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String file = sc.nextLine();
        try {
            Map<String, Integer> map = new HashMap<String, Integer>();
            List<String> list = Files.readAllLines(new File(file).toPath());
            String str1 = null;
            String newstr = "";
            String string[] = null;
            for (String str : list) {
                str1 = str.replaceAll("[,./!()+-?]", "");
                newstr += (str1+" ");
            }
            string = newstr.split(" ");
            Integer l = 0;
            for (int i = 0; i < string.length; i++) {
                l = map.get(string[i]);
                if (l == null) l = 1;
                else l++;
                map.put(string[i], l);
            }
            string=null;
            System.out.println(map);
        } catch (IOException e) {
            System.out.println("Sorry,file not founded! Check name or create file");
        }
    }
}
