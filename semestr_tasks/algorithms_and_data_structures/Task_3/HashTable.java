import java.util.LinkedList;

/**
 * Created by ��������� Vaio on 13.05.2015.
 */
public class HashTable implements MyHashTable {
    private int lenght=1000;
    private HashTableElement[] arrayHashTable = new HashTableElement[lenght];//array in which we put HashTableElement object

    public HashTableElement[] getArrayHashTable() {//HashTableElement getter
        return arrayHashTable;
    }

    public void setArrayHashTable(HashTableElement[] arrayHashTable) {//HashTableElement setter
        this.arrayHashTable = arrayHashTable;
    }

    @Override
    public void swap(HashTable b) {//swap two HashTable
        HashTableElement[] hashTableElements = arrayHashTable; //create buffer array
        arrayHashTable =b.getArrayHashTable();//put new array to this array
        b.arrayHashTable = hashTableElements;//put buffer array to new
    }

    @Override
    public void clear() {
        arrayHashTable = new HashTableElement[lenght];//clear table and HashTable
    }

    @Override
    public boolean earse(String key) {//delete element from key
        if (!contains(key)) {
            HashTableElement element = arrayHashTable[index(key)];
            if (!(element.getNext()==null)) {//if line with HashTableElemnt with such index have more then 1 element
             while (!(element.getNext() == null)) {
                    if (element.getNext().getKey().equals(key)) break;//find element with such key
                    element = element.getNext();
                }
                element.setNext(element.getNext().getNext());//redefinition of next element
            } else {arrayHashTable[index(key)]=null;} //delete hashtableelemnt with such index if in line only 1 element
            return true;
        }
        return false;
    }

    @Override
    public boolean insert(String key, Value v1) {//insert element
        HashTableElement hashTableElement = new HashTableElement(v1, key);//create new hashtablelement
        if (arrayHashTable[index(key)]==null) {//if no have element with such index put it in index
            arrayHashTable[index(key)] = hashTableElement;
            return true;
        } else {
            HashTableElement element = arrayHashTable[index(key)];//if in element with such index have line with elements tup new element to the end of line
            while (!(element.getNext()==null)) {
                if (element.getKey().equals(key)) {
                    element.setValue(v1);
                    return true;
                }
                element.getNext();
            }
            element.setNext(hashTableElement);
            return true;
        }
    }

    private int index(String key) {//create hashindex
        if (arrayHashTable.length!=0) {
        return Math.abs(key.hashCode())%arrayHashTable.length;}
        return 0;
    }

    @Override
    public boolean contains(String key) {//found equals key of new element
        if (arrayHashTable[index(key)]==null) return false;
        else {
            HashTableElement element = arrayHashTable[index(key)];
            while (!(element.getNext()==null)){
                if (element.getKey().equals(key)) return true;
                element = element.getNext();
            }
        }
        return false;
    }

    @Override
    public Value get(String key) { //return the key Value
         if (!contains(key)) {
            HashTableElement element = arrayHashTable[index(key)];
            while (!(element.getNext()==null)){
                if (element.getKey().equals(key)){ break;}
                element = element.getNext();
            }
             return element.getValue();
        }
        Value stvalue = new Value(-1, -1); //if element with this key not founded return and put element with key "null" and standard value
        insert("null", stvalue);
        return stvalue;
    }

    @Override
    public Value at(String key) {//return the key Value
        if (!contains(key)) {
            HashTableElement element = arrayHashTable[index(key)];
            while (!(element.getNext()==null)) {
                if (element.getKey().equals(key)) return element.getValue();
                element = element.getNext();
            }
        }
        return null;
    }

    @Override
    public int size() {//found size of HashTable
        int k = 0;
        for (int i = 0; i < arrayHashTable.length; i++) {
            if (!(arrayHashTable[i]==null)) {
                k++;
                HashTableElement element = arrayHashTable[i];
                while (!(element.getNext()==null)) {
                    element = element.getNext();
                    k++;
                }
            }
        }
        return k;
    }

    @Override
    public boolean empty() {//return true if HashTable is empty  and false if it has one or more elements
        if (size() != 0) return false;
        return true;
    }
}
