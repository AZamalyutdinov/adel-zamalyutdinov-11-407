/**
 * Created by ��������� Vaio on 13.05.2015.
 */
public interface MyHashTable {
    public void swap(HashTable b);
    public void clear();
    public  boolean earse(String key);
    public boolean insert(String key, Value v1);
    public boolean contains(String key);
    public Value get(String key);
    public Value at(String key);
    public int size();
    public boolean empty();
}
