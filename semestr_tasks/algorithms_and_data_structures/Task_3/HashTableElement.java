/**
 * Created by ����� on 14.04.2015.
 */
class HashTableElement { //Object of HashTableElement
    private Value value; //Object with stored in the hashTable
    private String key; //key of object
    private HashTableElement next = null; //if we have 2 or more elements with one hashcode we put link to second, third etc

    public HashTableElement(Value value, String key) {//Object constructor
        this.value = value;
        this.key = key;
    }

    public void setNext(HashTableElement next) {//next setter
        this.next = next;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Value getValue() {//value getter
        return value;
    }

    public String getKey() {//hashcode getter
        return key;
    }

    public HashTableElement getNext() {//next getter
        return next;
    }
}
