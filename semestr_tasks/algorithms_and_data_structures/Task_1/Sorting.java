import java.util.LinkedList;
import java.util.Random;

/*
 * Created by  on 24.02.2015.
 * Hello, dear user! At this module I will tell you about this program.
 * This program read array from *.txt file, put it in ordered doubly linked list and mixed this list.
 * To start program you need enter name of file (ex. input.txt) , which is filled with numbers.
 */
public class Sorting {
    public void mergeSort(LinkedList<Integer> linkedList, int low, int high) {
        if (low + 1 < high) {
            int middle = (low + high)/2; //finding middle of array
            mergeSort(linkedList, low, middle); //call recursion to left side of array
            mergeSort(linkedList, middle, high);//call recursion to right side of array

            int size = high - low; //finding size of array
            LinkedList<Integer> bufferLinkedList = new LinkedList<Integer>(); //create a buffer for merge
            int i = low;
            int j = middle;
            Random random = new Random();
            for (int k = 0; k < size; k++) {
                if (j >= high || i < middle && random.nextInt(2)==0) { //merge with random choice
                    bufferLinkedList.add(k,linkedList.get(i++));
                } else {
                    bufferLinkedList.add(k,linkedList.get(j++));
                }
            }
            for (int l=0; l<size;l++) { //copy and paste mixed array
                linkedList.remove(l + low);
                linkedList.add(l + low, bufferLinkedList.get(l));
            }
        }

    }
}