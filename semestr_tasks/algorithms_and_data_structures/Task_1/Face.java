import java.util.Scanner;

/*
 * Created by  on 24.02.2015.
 * Hello, dear user! At this module I will tell you about this program.
 * This program read array from *.txt file, put it in ordered doubly linked list and mixed this list.
 * To start program you need enter name of file (ex. input.txt) , which is filled with numbers.
 */
public class Face {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter name of file. Example: input.txt");
        String read = sc.nextLine();
        ListMotion listMotion = new ListMotion();
        listMotion.list(read);
    }
}
