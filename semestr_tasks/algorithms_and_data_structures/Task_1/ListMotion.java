import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

/*
 * Created by  on 24.02.2015.
 * Hello, dear user! At this module I will tell you about this program.
 * This program read array from *.txt file, put it in ordered doubly linked list and mixed this list.
 * To start program you need enter name of file (ex. input.txt) , which is filled with numbers.
 */
class ListMotion {
    public void list(String read) {
        Scanner sc = new Scanner(System.in);
        Integer n;
        Scanner scanner = null;
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        try {
            scanner = new Scanner(new FileInputStream(read)); //reading and regulating initial sequence
            while (scanner.hasNextInt()) {
                n = scanner.nextInt();
                if (linkedList.size() < 1)
                    linkedList.add(n);    //set element if in array only 1 element
                else if (n <= linkedList.get(0))
                    linkedList.add(0, n);  //set element in 1 place, if this element less than min
                else if (n >= linkedList.get(linkedList.size() - 1))
                    linkedList.add(n);    //set element in last place, if this element more than max
                else
                    for (int i = linkedList.size() - 1; i >= 0; i--)
                        if ((n < linkedList.get(i)) && (n >= linkedList.get(i - 1))) {
                            linkedList.add(i, n);  //finding setting element in i place, if element number (i-1) less than (i) and (i) less than (i+1)
                            break;
                        }
            }
            Sorting sorting = new Sorting();
            System.out.println("Result of program:");
            sorting.mergeSort(linkedList, 0, linkedList.size());  //calling a method of sorting in class Sorting
            for (int i = 0; i < linkedList.size(); i++)     //printing result of program
                System.out.print(linkedList.get(i) + " ");
        } catch (FileNotFoundException e) {
            System.out.println("File not funded. Please, check name or create file.");
        } finally {
            scanner.close();
        }
    }
}

