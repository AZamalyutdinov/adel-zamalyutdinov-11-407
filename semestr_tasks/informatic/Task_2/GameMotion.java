import java.util.LinkedList;
import java.util.Random;

/**
 * Created by Адель on 23.03.2015.
 */
public class GameMotion {
    private int score;
    private LinkedList<Integer> x = new LinkedList<Integer>(); //x coordinate of 0 array
    private LinkedList<Integer> y = new LinkedList<Integer>(); //y coordinate of 0 array

    public void setScore(int i) {
        score = i;
    } //score setter

    public int getScore() {
        return score;
    } //score getter

    public boolean combCheck(int[][] a) { //check can you make a motion, if  playing field is filled with numbers, which are not equal 0
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                if ((a[i][j] == a[i][j + 1]) || (a[j][i] == a[j + 1][i]) || x.size() != 0) return true;
            }
        }
        return false;
    }

    public boolean winCheck(int[][] a) { //if in playing field has 2048, return than user win
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if ((a[i][j] == 2048)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void goLeft(int[][] a) { //do left motion
        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 3; k++) {
                goMovie(a, i, k, 0, 1); //movie all number first time. This part useful in situation like this  x00x
                getSum(a, i, k, 0, 1); // sum elements
            }
            for (int k = 0; k < 3; k++) {
                for (int j = 3; j > k; j--) {
                    goMovie(a, i, j, 0, -1); //movie all elements in their new position
                }
            }
        }
    }

    public void goRight(int[][] a) { //do right motion
        for (int i = 0; i < 4; i++) {
            for (int k = 3; k > 0; k--) {
                goMovie(a, i, k, 0, -1); //movie and sum work like in goLeft method
                getSum(a, i, k, 0, -1); // sum elements
            }
            for (int k = 3; k > 0; k--) {
                for (int j = 0; j < k; j++) { //movie all elements in their new position
                    goMovie(a, i, j, 0, 1);
                }
            }
        }
    }

    public void goUp(int[][] a) { //do up motion
        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 3; k++) {
                goMovie(a, k, i, 1, 0); //movie and sum work like in goLeft method
                getSum(a, k, i, 1, 0);// sum elements
            }
            for (int k = 0; k < 3; k++) {
                for (int j = 3; j > k; j--) {
                    goMovie(a, j, i, -1, 0); //movie all elements in their new position
                }
            }
        }
    }

    public void goDown(int[][] a) { //do down motion
        for (int i = 0; i < 4; i++) {
            for (int k = 3; k > 0; k--) {
                goMovie(a, k, i, -1, 0); //movie and sum work like in goLeft method
                getSum(a, k, i, -1, 0);// sum elements
            }
            for (int k = 3; k > 0; k--) {
                for (int j = 0; j < k; j++) {
                    goMovie(a, j, i, 1, 0); //movie all elements in their new position
                }
            }
        }
    }

    public void goMovie(int[][] a, int i, int j, int ki, int kj) { //do movie. if next element equals 0, element a[i][j] movie to next position
        if (a[i + ki][j + kj] == 0) {
            a[i + ki][j + kj] = a[i][j];
            a[i][j] = 0;
        }
    }

    public void getSum(int[][] a, int i, int j, int ki, int kj) { //ki and kj - offset for finding the next element.
    // For example: ki=0; kj=1; Next element has x=i+0 -> x=i and y = j+1. So next element located in the same line on the right.
        //If ki=-1; kj=0 next element located in the same column from the top
        if ((a[i + ki][j + kj] == a[i][j]) && (a[i][j] != 0)) { //if elements not equal 0 and first element equals second method sum them
            a[i][j] += a[i + ki][j + kj]; //sum first and second element and put result to first element position
            a[i + ki][j + kj] = 0;
            score += a[i][j]; //change score
        }
    }

    public void getNew(int[][] a) { //create new elements
        x.clear(); //clear previous list
        y.clear(); //clear previous list
        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (a[i][j] == 0) {
                    x.add(i); //create new lists, which all elements equal x and y coordinate of 0 element in playing field
                    y.add(j);
                }
            }
        }
        if (x.size() != 0) {
            int i = 0; //if we have only one 0 in playing field add only one new element
            if (x.size() != 1) {
                i = random.nextInt(x.size()); //chose a random element of x and y list
                a[x.get(i)][y.get(i)] = 2;
            }
            i = random.nextInt(x.size()); //if we have two or more 0 add second new element
            a[x.get(i)][y.get(i)] = 2;
        }
    }

    public void print(int[][] a) { //print playing field
        for (int i = 0; i < 4; i++) {
            System.out.println("|----|----|----|----|");
            System.out.print("|");
            for (int j = 0; j < 4; j++) {
                if (a[i][j] / 10 == 0) System.out.print("   " + a[i][j] + "|");
                else if (a[i][j] / 100 == 0) System.out.print("  " + a[i][j] + "|");
                else if (a[i][j] / 1000 == 0) System.out.print(" " + a[i][j] + "|");
                else if (a[i][j] / 10000 == 0) System.out.print(a[i][j] + "|");
            }
            System.out.println();
        }
        System.out.println("|----|----|----|----|");
        System.out.println("Score " + score);
    }
}