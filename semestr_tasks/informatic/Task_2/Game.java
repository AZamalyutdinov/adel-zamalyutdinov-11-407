import java.io.*;
import java.util.Scanner;

/**
 * Created by Адель on 25.03.2015.
 */
public class Game {
    private int[][] a = new int[4][4]; //playing field
    private GameMotion gameMotion = new GameMotion();

    public void readGame() { //read saved game from file
        Scanner scanner = null;
        try {
            scanner = new Scanner(new FileInputStream("SaveGame.txt"));
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    a[i][j] = scanner.nextInt();//read array from int line
                }
            }
            gameMotion.setScore(scanner.nextInt()); //last element in file is score of this game
            game(); //after loading game start play
        } catch (FileNotFoundException e) {
            System.out.println("Sorry, saved game not founded. Start new game");
        }
    }

    public void newGame() throws IOException { //started new game
        gameMotion.setScore(0); //set 0 score
        gameMotion.getNew(a); //set new elements
        game();
    }

    public void game() throws FileNotFoundException { //create movie
        Scanner sc = new Scanner(System.in);
        gameMotion.print(a); //print field
        String s;
        do { //read input line and do command
            s = sc.nextLine(); //read command
            switch (s) {
                case "w":
                    gameMotion.goUp(a); //do up motion
                    gameMotion.getNew(a); //set new elements
                    break;
                case "a":
                    gameMotion.goLeft(a);//do left motion
                    gameMotion.getNew(a);//set new elements
                    break;
                case "s":
                    gameMotion.goDown(a);//do down motion
                    gameMotion.getNew(a);//set new elements
                    break;
                case "d":
                    gameMotion.goRight(a);//do right motion
                    gameMotion.getNew(a);//set new elements
                    break;
                case "exit": //exit from game
                    continue;
                default:
                    System.out.println("error");
                    break;
            }
            gameMotion.print(a); //print field
            if (gameMotion.combCheck(a) == false) { //Game over check and message
                System.out.println("Game over. Your score: " + gameMotion.getScore());
                break;
            }
            if (gameMotion.winCheck(a) == true) { //Win check and message
                System.out.println("Congratulations! You are win! Your score: " + gameMotion.getScore());
                break;
            }
        }
        while (!s.equals("exit")); //stop game
        Score score = new Score(); //create object type of Score
        score.putNewScore(gameMotion.getScore()); //set new high score
        PrintWriter print = new PrintWriter(new FileOutputStream("SaveGame.txt")); //save last game and score to file
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                print.print(a[i][j] + " "); //save field in int line
            }
        }
        print.print(gameMotion.getScore()); //save score
        print.flush();
        print.close();
    }
}