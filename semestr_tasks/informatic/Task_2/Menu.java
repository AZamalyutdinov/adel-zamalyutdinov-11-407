import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by Адель on 25.03.2015.
 */
public class Menu {
    public static void main(String[] args) throws IOException, InterruptedException {
        Scanner sc = new Scanner(System.in);
        Game game = new Game(); //creating object of Game class
        Score score = new Score(); //creating object of Score class
        int s; //case index
        do { //game menu
            System.out.println("Choose any item:");
            System.out.println("1. Continue last game");
            System.out.println("2. Play new game");
            System.out.println("3. High score");
            System.out.println("4. Information about game");
            System.out.println("5. Exit");
            s = sc.nextInt();
            switch (s) {
                case 1:
                    System.out.println("To move to left put a, right - d, up - w, down - s."); //reminder of game keys
                    game.readGame(); //start saved game
                    break;
                case 2:
                    System.out.println(printText(1));
                    TimeUnit.SECONDS.sleep(20); //wait 20 seconds. In this time user read first rules
                    game.newGame(); //start new game
                    break;
                case 3:
                    score.printHighScore(); //print top 5 of high score
                    break;
                case 4:
                    System.out.println(printText(2)); //print information about game
                    break;
                case 5:
                    break; //if entered 5 program break switch and go out from cycle
                default:
                    System.out.println("Wrong number. Try again"); //print "exception' if user entered other number
            }
        } while (s != 5); //exit from all game
    }
        public static String printText(int i) throws FileNotFoundException { //read info text about rules and game from file
            Scanner scanner = new Scanner(new FileInputStream("TextInfo.txt"));
            if (i==1) return scanner.nextLine(); //return basic information  about game
            else {
                scanner.nextLine();
                return scanner.nextLine(); //return historical note about game
            }
        }
}
