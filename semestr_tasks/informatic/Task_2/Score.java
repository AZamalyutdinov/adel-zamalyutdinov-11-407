import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by Адель on 25.03.2015.
 */
public class Score {
    private LinkedList<Integer> score = new LinkedList<Integer>();//list of top 5 score
    private LinkedList<String> name = new LinkedList<String>();//list of top 5 names

    public void printHighScore() throws FileNotFoundException { //print to console high score
        putScore();
        System.out.println(name.size());
        for (int i = 0; i < name.size(); i++) {
            System.out.println((i + 1) + ". " + name.get(i) + " " + score.get(i));
        }
        score.clear();
        name.clear();
    }

    public void putScore() throws FileNotFoundException { //put last high score list. File has a form like 5 results and 5 name
        Scanner scanner = new Scanner(new FileInputStream("HighScore.txt"));
        while (scanner.hasNextInt()) {//read score array
            score.add(scanner.nextInt());
        }
        while (scanner.hasNextLine()) { //read name array
            String[] list = null;
            list = scanner.nextLine().split(" "); //split line to 5 sublines
            for (int i = 1; i < list.length; i++) {
                name.add(list[i]);
            }
        }
    }

    public void putNewScore(Integer score1) throws FileNotFoundException { //put new high score
        putScore();
        int k = -1;
        Scanner sc = new Scanner(System.in);
        if (score1 >= score.get(0)) { //if user score is in 5 top, put it in i position
            k = 0;
        } else
            for (int i = 1; i < score.size(); i++) {//find position of new top-5 score
                if ((score1.compareTo(score.get(i))==1) && (score1.compareTo(score.get(i-1))==-1)||score1.equals(score.get(i))) {
                    k = i;
                }
            }
        System.out.println("Congratulations! You are in 5 top! Please enter your name."); //write name and score to k position
        if (k != -1) {
            score.add(k, score1); //put high score and name in their place
            name.add(k, sc.nextLine());
            score.removeLast(); //delete last (high score #6)
            name.removeLast();
        }
        PrintWriter print = new PrintWriter(new FileOutputStream("HighScore.txt")); //write score and name list to file
        for (int i = 0; i <score.size() ; i++) {//first being recorded score
            print.print(score.get(i)+" ");
        }
        for (int i = 0; i <name.size() ; i++) {//then recorded the names of owners of high score
            print.print(name.get(i) + " ");
            System.out.println((i+1)+". "+name.get(i)+" "+score.get(i));
        }
        score.clear(); //after recording clear lists
        name.clear();
        print.flush();
        print.close();

    }
}