import java.util.ArrayList;
import java.util.Scanner;

/**
 Hello, dear user! At this module I will tell you about this program. This is main part of program. Program work only with FindString.java file.
 * This program read array from *.txt file, read substring from console  and found it. The result of program is array of substring entry index.
 * You can check result by hand, if you are not lazy. But all my test have shown that program work correct =).
 * To start program you need enter name of file (ex. input.txt) , which is filled with numbers, and enter substring
 */
class LinePross {
    public void indexList(String read) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter line, witch you want find:");
        String findLine = sc.nextLine(); //read line, witch need find
        FindString findString = new FindString();
        ArrayList<Integer> indexList = new ArrayList<Integer>();
        indexList = findString.findString(read, findLine); //Put in indexList results of method findString
        if (indexList.size() == 0) {
            System.out.println("Sorry, this substring not founded. You will be more lucky at next time!=)");
        } else {
            System.out.println("Line " + findLine + " enter in text with this(these) index(s):");
            for (int i = 0; i < indexList.size(); i++) {
                System.out.print(indexList.get(i) + "; "); //print list
            }
            System.out.println();
            System.out.println("P.S. Count of index goes from 0");
        }
    }
}
