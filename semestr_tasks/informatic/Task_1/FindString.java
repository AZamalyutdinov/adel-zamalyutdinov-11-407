import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/*
*This class find substring in text.
* In do it with help of suffix array and binary search.
* Mare about code You can read in comments.
*/
public class FindString {
    public ArrayList<Integer> findString(String read, String findline){
        Integer element;
        Scanner scanner = null;
        ArrayList<String> arrayList = new ArrayList<String>(); //suffix array
        List<String> buffarrayList = new ArrayList<String>(); //buffer array to find line
        ArrayList<Integer> indexList = new ArrayList<Integer>(); //array of entry index
        Map map = new HashMap<String,Integer>(); //Map of suffix. Key of map is suffix line. Value is entry index
        String line = "";
        try {
            scanner = new Scanner(new FileInputStream(read));
            while (scanner.hasNextLine()) {
                line+=(scanner.nextLine()+" "); //read all lines and put thew in to one line
            }
            arrayList.add(line); //put all text in position 0
            map.put(line,0);
            for (int i = 0; i <line.length()-1 ; i++) {
                arrayList.add(arrayList.get(i).substring(1));  //put line without first letter
                map.put(arrayList.get(i+1),i+1); //put this line in to map
            }
            Integer index=0;
            Collections.sort(arrayList); //sort with standard Java sort
            for (int i = 0; i <arrayList.size(); i++) {
                if (arrayList.get(i).length()>findline.length())
                    buffarrayList.add(arrayList.get(i).substring(0,findline.length())); //if length of suffix more than length of desired line we put in buffer array
                    // only n first letter, where n - length of line. This this command we can find accurate position of desired line.
                else buffarrayList.add(arrayList.get(i));
                index = Collections.binarySearch(buffarrayList, findline); //put result of binary search. Each time we search again. This command can found each enter of substring
                if ((Collections.binarySearch(indexList,index)<0)&&(index>=0))
                    indexList.add(index); //if this index positive and we hasn't found index yet we put it in indexList
            }
            index=0;
            for (int j = 0; j <indexList.size() ; j++) {
                index= (Integer) map.get(arrayList.get(indexList.get(j))); //get final index of substring. Put index, witch corresponds index of substring
                indexList.remove(j);
                indexList.add(j,index); //put final index
            }
            Collections.sort(indexList); //brings beauty to index array
        } catch (IOException e) {
            System.out.println("File not founded. Please, check name of file or create it");
        }
        return indexList; //return final list
    }
}
