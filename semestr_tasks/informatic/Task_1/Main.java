import java.util.Scanner;

/**
 Hello, dear user! At this module I will tell you about this program. This is main part of program. Program work only with FindString.java file.
 * This program read array from *.txt file, read substring from console  and found it. The result of program is array of substring entry index.
 * You can check result by hand, if you are not lazy. But all my test have shown that program work correct =).
 * To start program you need enter name of file (ex. input.txt) , which is filled with numbers, and enter substring
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter name of file. Example: input.txt");
        String read = sc.nextLine(); //read file
        LinePross linePross = new LinePross();
        linePross.indexList(read);
    }
}
