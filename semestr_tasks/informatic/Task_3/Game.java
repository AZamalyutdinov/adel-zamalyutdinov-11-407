import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Адель on 02.05.2015.
 */
public class Game {
    private ArrayList<Ship> arrayShips = new ArrayList<Ship>();//array list of ships
    private String ip;//ip with player connected
    private Field field = new Field();//create field
    private DatagramSocket datagramSocket; //datagram socket creator
    private boolean win = false;//win status

    public boolean loseCheck() {//check game status
        for (int i = 0; i < arrayShips.size(); i++) {
            if (arrayShips.get(i).getStatus().equals(Ship.ShootResult.Missed) || arrayShips.get(i).getStatus().equals(Ship.ShootResult.Wounded)) {
                return false; //return false in in field has missed ship or wounded ship
            }
        }
        return true;
    }
    public void readFiledFormFile() throws FileNotFoundException {
        char[][] fieldShip = new char[10][10];
        Scanner scanner = new Scanner(new FileInputStream("field.txt"));
        int i = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            for (int j = 0; j < line.length(); j++) {
                if (line.charAt(j) == '*')
                    fieldShip[i][j] = line.charAt(j);
                else fieldShip[i][j] = ' ';
            }
            i++;
        }
        for (int j = 0; j <10 ; j++) {
            for (int k = 0; k <10 ; k++) {
                String line = "";
                if (fieldShip[j][k]=='*') {
                    if(j+1<10&&fieldShip[j+1][k]=='*') {
                        i=j-1;
                        while (fieldShip[++i][k]=='*'&&i<10) {line=line+(char)(i+(int)'A')+k; fieldShip[i][k]=' ';}
                    } else
                    if(k+1<10&&fieldShip[j][k+1]=='*') {
                        i=k-1;
                        while (fieldShip[j][++i]=='*'&&i<10) {line=line+(char)(j+(int)'A')+i; fieldShip[j][i]=' ';}
                    } else {line=line+(char)(j+(int)'A')+k; fieldShip[j][k]=' ';}
                }
                if (!line.equals("")) {createShip(line);}
            }
        }
        field.printFields();
    }

    public void createShip(String line) {//ship creator
        int size = line.length() / 2; //define ship size
        String pos = "h";//create standard ship position
        if ((size != 1) && (line.charAt(0) == line.charAt(2)))
            pos = "v"; // if first and second coordinates in one column set vertical position
        int x = (int) line.charAt(0) - (int) 'A' + 1;//char to int coordinate translator
        int y = (int) line.charAt(1) - (int) '0' + 1;//char to int coordinate translator
        Ship ship = new Ship(x, y, size, pos);
        arrayShips.add(ship);
        field.myField(ship);
    }


    public String getFire(String c) throws IOException {//take fire
        int i = 0;
        int x = (int) c.charAt(0) - (int) 'A' + 1;//char to int coordinate translator
        int y = (int) c.charAt(1) - (int) '0' + 1;//char to int coordinate translator
        Ship.ShootResult shoot = Ship.ShootResult.Missed;//set standard shoot result
        while ((!(shoot.equals(Ship.ShootResult.Wounded) || shoot.equals(Ship.ShootResult.Killed))) && (i < 10)) {
            shoot = arrayShips.get(i++).getShootResult(y, x);//find hit in ship
        }
        x += (int) 'A' - 1;//preparation for int to char translate
        return ((char) x + ((y - 1) + shoot.toString()));//translate int to  char and put it in one string and return
    }

    /**
     *
     * @throws SocketException
     */
    public void connection() throws SocketException {//connect with another player
        datagramSocket = new DatagramSocket(6890);//create new datagram socket
        datagramSocket.connect(new InetSocketAddress(ip, 6890));// datagram socket connecting people
    }

    public void send(String packet) throws IOException {//send packet of information (string)
        DatagramPacket datagramPacket = new DatagramPacket(packet.getBytes(), packet.getBytes().length);//new datagram packet creator
        datagramSocket.send(datagramPacket);//send packet
    }

    public void adopt() throws IOException {//receive a packet with information
        DatagramPacket datagramPacket = new DatagramPacket(new byte[4], 4); //new datagram packet creator
        datagramSocket.receive(datagramPacket);//receive packet
        String s = new String(datagramPacket.getData(), "UTF-8");//byte[] to String in UTF-8
        if (s.substring(2, 4).equals("Mi")) {//processing of received information
            field.setFieldChanges(s.charAt(0), s.charAt(1), Ship.ShootResult.Missed);////set changes in enemy field
        } else if (s.substring(2, 4).equals("Wo")) {
            field.setFieldChanges(s.charAt(0), s.charAt(1), Ship.ShootResult.Wounded);////set changes in enemy field
        } else if (s.substring(2, 4).equals("Ki")) {
            field.setFieldChanges(s.charAt(0), s.charAt(1), Ship.ShootResult.Killed);//set changes in enemy field
        } else if (s.equals("lose")) {
            this.win = true;//in received lose status of enemy make your status win
        } else {
            String result = getFire(s.substring(0, 2));//else get fire
            send(result);//send result
            if (result.substring(2).equals("Wounded") || result.substring(2).equals("Killed")) {//set changes in my field
                field.getMyField()[(int) s.charAt(1) - (int) '0' + 1][(int) s.charAt(0) - (int) 'A' + 1] = 'X';
            } else {
                field.getMyField()[(int) s.charAt(1) - (int) '0' + 1][(int) s.charAt(0) - (int) 'A' + 1] = '*';
            }
        }
        field.printFields();//print fields
    }

    public boolean getWin() {//win status getter
        return win;
    }


    public void setIp(String ip) {//ip setter
        this.ip = ip;
    }
}
