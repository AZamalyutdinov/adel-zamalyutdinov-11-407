import java.util.ArrayList;
/**
 * Created by Адель on 02.05.2015.
 */
public class Field {//graphical representation of field
    private char[][] enemyField = field();//enemy field, including layout of field
    private char[][] myField = field();//player field, including layout of field

    public char[][] getMyField() {
        return myField; //player field getter
    }

    public char[][] field() { // layout of field creator
        char[][] field = new char[11][11];
        Integer i = 0;
        for (char c = 'A'; c <= 'J'; c++) {
            field[0][i + 1] = c; //create alphabetic coordinate
            field[i + 1][0] = i.toString().charAt(0); //create number coordinate
            i++;
        }
        for (int j = 1; j <11 ; j++) {
            for (int k = 1; k <11 ; k++) {
                field[j][k] = ' '; //create empty value
            }

        }
        return field;
    }
    public void printFields() {//field printer
        System.out.println("Your field                                              Enemy field");
        System.out.println("|---|---|---|---|---|---|---|---|---|---|---|          |---|---|---|---|---|---|---|---|---|---|---|");
        for (int i = 0; i <11 ; i++) {
            for (int j = 0; j <11 ; j++) {
                System.out.print("| "+myField[i][j]+" ");
            }
            System.out.print("|          ");
            for (int j = 0; j <11 ; j++) {
                System.out.print("| "+enemyField[i][j]+" ");
            }
            System.out.println("|");
            System.out.println("|---|---|---|---|---|---|---|---|---|---|---|          |---|---|---|---|---|---|---|---|---|---|---|");
        }
    }
    public boolean myField(Ship ship) {//put ship in coordinate field
        for (int i = 0; i < ship.getSize(); i++) {// put in field # if this coordinate owned ship
            myField[ship.getX() + i * ship.getDx()][ship.getY() + i * ship.getDy()] = '#';
        }
        return true;
    }

    public void setFieldChanges(char x, char y, Ship.ShootResult result) {//set changes in enemy field
        int x1 = (int) x - (int) 'A'+1;//translate char coordinate to int coordinate
        int y1 = (int) y - (int) '0' + 1; //translate char coordinate to int coordinate
        if (result.equals(Ship.ShootResult.Missed)) {
            enemyField[y1][x1] = '*'; //set sing indicating missed shoot result
        } else if (result.equals(Ship.ShootResult.Wounded)) {
            enemyField[y1][x1] = 'X';//set sing indicating wounded shoot result
        } else if (result.equals(Ship.ShootResult.Killed)) {
            enemyField[y1][x1] = 'X';//set sing indicating killed shoot result
        }
    }
}
