import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.util.Scanner;

/**
 * Created by Адель on 02.05.2015.
 */
public class Players {
    private static Game game = new Game();
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        System.out.println("Please. determinant number of your player. If you are player 1 enter 1, and enter 2 if you are player 2");
        String i = sc.nextLine();
        System.out.println("Please, enter IP");
        game.setIp(sc.nextLine());
        game.readFiledFormFile();
        game.connection();
        if (i.equals("1")) {
            player1();
            System.out.println("p1");
        } else {
            player2();
            System.out.println("p2");
        }
    }

    private static void player1() throws IOException {
        while (!game.getWin()) {
            game.send(sc.nextLine());
            game.adopt();
            if (game.loseCheck()) {
                game.send("lose");
                System.out.println("Sorry, you lose=(");
                break;
            }
            game.adopt();
        }
        if (game.getWin())
            System.out.println("Congratulations, you win!=) ");
    }

    private static void player2() throws IOException {
        while (!game.getWin()) {
            game.adopt();
            if (game.loseCheck()) {
                game.send("lose");
                System.out.println("Sorry, you lose=(");
                break;
            }
            game.send(sc.nextLine());
            game.adopt();
        }
        if (game.getWin())
            System.out.println("Congratulations, you win!=) ");
    }
}
