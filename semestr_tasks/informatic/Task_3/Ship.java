/*
 * Created by Адель on 02.05.2015.
 */
public class Ship {
    private int x; //ship first x-coordinate
    private int dx = 0; //direction in x-coordinates
    private int y;//ship first y-coordinate
    private int dy = 0;//direction in y-coordinates
    private int size;//size of ship (1,2,3 or 4)
    private int dsize; //size of living part of ship
    public ShootResult status = ShootResult.Missed;//ship status

    public Ship(int x, int y, int size, String orient) {//ship constructor
        this.x = x;
        this.y = y;
        this.size = size;
        this.dsize = size;
        if (orient.equals("h")) {//direction translator
            this.dx = 1; //if ship directed on horizontal put direction in x-coordinates value 1
        } else {
            this.dy = 1;//else put direction in y-coordinates this value
        }
    }

    public ShootResult getShootResult(int x1, int y1) {//shoot reuslt
        for (int i = 0; i < size; i++) {
            if (((x + dx * i == x1) && (y == y1)) || ((y + dy * i == y1) && (x == x1))) {//search coordinate
                if (dsize == 1) {
                    status = ShootResult.Killed;//if ship has this coordinate and its dsize equals 1 we killed this ship
                    return status;
                } else {
                    if ((status.equals(ShootResult.Missed)) || (status.equals(ShootResult.Wounded))) {
                        status = ShootResult.Wounded; //if ship has this coordinate we wonded
                        dsize--;//reduction of living part of ship
                        return status;
                    }
                }
            }
        }
        return ShootResult.Missed;
    }

    enum ShootResult {//shoot result enum
        Missed, Wounded, Killed
    }

    public int getX() {//x-coordinate getter
        return x;
    }

    public int getY() {//y-coordinate getter
        return y;
    }

    public ShootResult getStatus() {//shoot status getter
        return status;
    }

    public int getDx() {//direction in x-coordinates getter
        return dx;
    }

    public int getDy() {//direction in y-coordinates getter
        return dy;
    }

    public int getSize() {//size getter
        return size;
    }

}