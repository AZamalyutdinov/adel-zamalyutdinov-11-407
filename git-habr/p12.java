import java.util.*;

public class p12 {
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int n = sc.nextInt();
		int[][] a = new int [n][n];
		
		for(int i=0, k=1, j, p=0; i<n*n; ++k, ++i) {
			for (j=k-1; j<n-k+1; ++j) {
				a[k-1][j] = ++p;
			}
			for (j=k; j<n-k+1; ++j) {
				a[j][n-k] = ++p;
			}
			for (j=n-k-1; j>=k-1; --j) {
				a[n-k][j] = ++p;
			}
			for (j=n-k-1; j>=k; --j) {
				a[j][k-1] = ++p;
			}
		}
 
		for(int y=0; y<n; ++y) {
			for(int x=0; x<n; ++x) {
				System.out.print(" " + a[x][y]);				
			}
			System.out.println();
		}
		
	}
}