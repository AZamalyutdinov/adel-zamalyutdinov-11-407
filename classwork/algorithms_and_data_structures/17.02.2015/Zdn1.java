/**
 * Created by Адель on 17.02.2015.
 */
import java.util.*;

public class Zdn1 {
    public static void main(String[] args) {
        ArrayList<MyClass> list = new ArrayList<MyClass>();
        list.add(new MyClass(0,1));
        list.add(new MyClass(1,1));
        for (int i=1;i<10;i++) {
            for (int j=0;j<list.size()-1;j++) {
                int newp = list.get(j).getP()+list.get(j+1).getP();
                int newq = list.get(j).getQ()+list.get(j+1).getQ();
                if (newq<=10) {
                    list.add(j+1,new MyClass(newp,newq));
                }
            }
        }
        for (MyClass c: list) { System.out.print(c.getP()+"/"+c.getQ()+" ; ");
        }
    }

    static class MyClass {
        private int q;
        private int p;
        MyClass(int p,int q) {
            this.q=q;
            this.p=p;
        }
        int getP() {return p;}
        int getQ() {return q;}
    }
}

