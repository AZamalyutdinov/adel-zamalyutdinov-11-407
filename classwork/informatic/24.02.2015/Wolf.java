/**
 * Created by Адель on 24.02.2015.
 */
public class Wolf extends Animal {
    @Override
    public void food(Food x) {
        if (x==Food.grass) {throw new FoodException();}
        System.out.print("Я волк, ");
        super.food(x);
    }
}
